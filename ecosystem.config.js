module.exports = {
    apps : [{
        name: "franchise-hockey-app",
        instances  : 5,
        exec_mode  : "cluster",
        script: "bin/www",
        listen_timeout : 3000,
        kill_timeout : 3000,
        env: {
            NODE_ENV: "development",
            PORT: 3000
        },
        env_test: {
            NODE_ENV: "test",
            PORT: 3000
        },
        env_stage: {
            NODE_ENV: "stage",
            PORT: 8080
        },
        env_production: {
            NODE_ENV: "production",
            PORT: 8080
        }
    }]
};
