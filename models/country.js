'use strict';

module.exports = (sequelize, DataTypes) => {
    const Country = sequelize.define('Country', {
        title: DataTypes.STRING
    });
    
    Country.associate = function(models) {
        models.Country.hasMany(models.Player);
        models.Country.hasMany(models.Team);
    };
    
    return Country;
};
