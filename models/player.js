'use strict';

module.exports = (sequelize, DataTypes) => {
    const Player = sequelize.define('Player', {
        name: { type: DataTypes.STRING, allowNull: false },
        surname: { type: DataTypes.STRING, allowNull: false },
        birthdate: { type: DataTypes.DATE, allowNull: false },
        status: { type: DataTypes.ENUM, allowNull: true ,
            values: ['active player', 'ended', ''] },
        gender: { type: DataTypes.STRING, allowNull: true },
        nationality: { type: DataTypes.STRING, allowNull: true },
        height: { type: DataTypes.STRING, allowNull: true },
        weight: { type: DataTypes.STRING, allowNull: true },
        shoots: { type: DataTypes.ENUM, allowNull: true ,
            values: ['left', 'right'] }
    }, {
        getterMethods : {
            fullName : function()  {
                return this.name + ' ' + this.surname;
            }
        },
        setterMethods   : {
            fullName : function(value) {
                const names = value.split(' ');
            
                this.setDataValue('name', names.slice(0, -1).join(' '));
                this.setDataValue('surname', names.slice(-1).join(' '));
            },
        }
    });
    
    Player.associate = function(models) {
        models.Player.belongsTo(models.Country);
        models.Player.hasMany(models.Team);
    };
    
    return Player;
};
