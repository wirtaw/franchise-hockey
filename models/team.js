'use strict';

module.exports = (sequelize, DataTypes) => {
    const Team = sequelize.define('Team', {
        title: DataTypes.STRING
    });
    
    Team.associate = function(models) {
        models.Team.belongsTo(models.Country);
    };
    
    return Team;
};
