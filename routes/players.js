const express = require('express');
const router = express.Router();

const models = require('../models');

/* GET players listing. */
router.get('/', function(req, res) {
  models.Player.findAll({
    include: [ models.Country, models.Team ]
  }).then((players) => {
    res.send({
      success: true,
      data: players
    });
  }).catch((error) => {
    // console.log(`Error ${error}`);
    res.send({
      success: false,
      message: (error.errors[0].message) ? error.errors[0].message : ''
    });
  });
});

/* POST new player */
router.post('/create', function(req, res) {
  models.Player.create({
    name: req.body.name,
    surname: req.body.surname,
    birthdate: req.body.birthdate,
    status: req.body.status,
    gender: req.body.gender,
    nationality: req.body.nationality,
    height: req.body.height,
    weight: req.body.weight,
    shoots: req.body.shoots
  }).then((player) => {
    res.send({
      success: true,
      data: player
    });
  }).catch((error) => {
    // console.log(`Error ${error.errors[0].message}`);
    res.send({
      success: false,
      message: (error.errors[0].message) ? error.errors[0].message : ''
    });
  });
});

/* PATCH exist player */
router.patch('/:id/update', function(req, res) {
  models.Player.update({
    name: req.body.name,
    surname: req.body.surname,
    birthdate: req.body.birthdate,
    status: req.body.status,
    gender: req.body.gender,
    nationality: req.body.nationality,
    height: req.body.height,
    weight: req.body.weight,
    shoots: req.body.shoots
  },{
    where: {
      id: parseInt(req.params.id)
    }
  }).then(() => {
    res.send({
      success: true
    });
  }).catch((error) => {
    // console.log(`Error ${error.errors[0].message}`);
    res.send({
      success: false,
      message: (error.errors[0].message) ? error.errors[0].message : ''
    });
  });
});

/* DELETE player */
router.delete('/:id/delete', function(req, res) {
  models.Player.drop({},{
    where: {
      id: parseInt(req.params.id)
    }
  }).then(() => {
    res.send({
      success: true
    });
  }).catch((error) => {
    // console.log(`Error ${error.errors[0].message}`);
    res.send({
      success: false,
      message: (error.errors[0].message) ? error.errors[0].message : ''
    });
  });
});

module.exports = router;
