//During the test the env variable is set to test
process.env.NODE_ENV = 'test';
/* eslint-env mocha */

//Require the dev-dependencies
let chai = require('chai');
let chaiHttp = require('chai-http');
let server = require('../app');
chai.should();

chai.use(chaiHttp);
//Our parent block
describe('App', () => {
    beforeEach((done) => { // Before each test we empty the database
        done();
    });
    afterEach((done) => { // After each test we empty the database
        done();
    });
    /*
      * Test the /GET route
      */
    describe('/GET ', () => {
        it('it should GET main page', (done) => {
            chai.request(server)
                .get('/')
                .end((err, res) => {
                    
                    res.should.have.status(200);
                    res.text.should.be.a('string');
                    done();
                });
        });
    });
    
});
