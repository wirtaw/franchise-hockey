//During the test the env variable is set to test
/* eslint-env mocha */
process.env.NODE_ENV = 'test';

const models = require('../models');

//Require the dev-dependencies
let chai = require('chai');
let chaiHttp = require('chai-http');
let server = require('../app');
chai.should();

chai.use(chaiHttp);

const cleanAndSync = new Promise((resolve, reject) => {
    if (models.sequelize) {
        resolve(models.sequelize.drop());
    } else {
        reject();
    }
});

//Our parent block
describe('Players', () => {
    beforeEach((done) => { // Before each test we empty the database
        cleanAndSync.then(() => {
            models.sequelize['sync']().then(function() {
                // console.log('beforeEach sync');
                done();
            }).catch(function() {
                // console.log('beforeEach sync error');
                done();
            });
        }).catch(() => {
            done();
        });
    });
    afterEach((done) => { // After each test we empty the database
        cleanAndSync.then(() => {
            models.sequelize['sync']().then(function() {
                // console.log('beforeEach sync');
                done();
            }).catch(function() {
                // console.log('beforeEach sync error');
                done();
            });
        }).catch(() => {
            done();
        });
    });
    /*
      * Test the /GET route
      */
    describe('/GET Players', () => {
        it('it should GET players', (done) => {
            chai.request(server)
                .get('/players')
                .end((err, res) => {
                    
                    res.should.have.status(200);
                    res.body.should.be.a('object');
                    res.body.success.should.be.a('boolean');
                    res.body.success.should.be.eql(true);
                    res.body.data.should.be.a('array');
                    res.body.data.length.should.be.eql(0);
                    done();
                });
        });
    });
    
    
    /*
      * Test the /POST route
      */
    describe('/POST Player', () => {
        it('it should POST player', (done) => {
            const player = {
                name: 'Name',
                surname: 'Surname',
                birthdate: '1999-01-01',
                status: 'Active',
                gender: 'Male',
                nationality: 'Canada',
                height: '185 cm',
                weight: '85 kg',
                shoots: 'Left'
            };
            chai.request(server)
                .post('/players/create')
                .send(player)
                .end((err, res) => {
                    res.should.have.status(200);
                    res.body.should.be.a('object');
                    res.body.success.should.be.a('boolean');
                    res.body.success.should.be.eql(true);
                    res.body.data.should.be.a('object');
                    res.body.data.name.should.be.a('string');
                    res.body.data.name.should.be.eql(player.name);
                    done();
                });
        });
    });
    
    /*
      * Test the /POST route
      */
    describe('/POST Player', () => {
        it('it should not POST player without name', (done) => {
            const player = {
                surname: 'Surname',
                birthdate: '1999-01-01',
                status: 'Active',
                gender: 'Male',
                nationality: 'Canada',
                height: '185 cm',
                weight: '85 kg',
                shoots: 'Left'
            };
            chai.request(server)
                .post('/players/create')
                .send(player)
                .end((err, res) => {
                    // console.log(JSON.stringify(res.body, undefined, 2));
                    res.should.have.status(200);
                    res.body.should.be.a('object');
                    res.body.success.should.be.a('boolean');
                    res.body.success.should.be.eql(false);
                    res.body.message.should.be.a('string');
                    res.body.message.should.be.eql('Player.name cannot be null');
                    done();
                });
        });
    });
    
    /*
      * Test the /POST route
      */
    describe('/POST Player', () => {
        it('it should not POST player without second name', (done) => {
            const player = {
                name: 'Name',
                birthdate: '1999-01-01',
                status: 'Active',
                gender: 'Male',
                nationality: 'Canada',
                height: '185 cm',
                weight: '85 kg',
                shoots: 'Left'
            };
            chai.request(server)
                .post('/players/create')
                .send(player)
                .end((err, res) => {
                    // console.log(JSON.stringify(res.body, undefined, 2));
                    res.should.have.status(200);
                    res.body.should.be.a('object');
                    res.body.success.should.be.a('boolean');
                    res.body.success.should.be.eql(false);
                    res.body.message.should.be.a('string');
                    res.body.message.should.be.eql('Player.surname cannot be null');
                    done();
                });
        });
    });
    
    /*
      * Test the /POST route
      */
    describe('/POST Player', () => {
        it('it should not POST player without birthdate', (done) => {
            const player = {
               name: 'Name',
                surname: 'Surname',
                status: 'Active',
                gender: 'Male',
                nationality: 'Canada',
                height: '185 cm',
                weight: '85 kg',
                shoots: 'Left'
            };
            chai.request(server)
                .post('/players/create')
                .send(player)
                .end((err, res) => {
                    // console.log(JSON.stringify(res.body, undefined, 2));
                    res.should.have.status(200);
                    res.body.should.be.a('object');
                    res.body.success.should.be.a('boolean');
                    res.body.success.should.be.eql(false);
                    res.body.message.should.be.a('string');
                    res.body.message.should.be.eql('Player.birthdate cannot be null');
                    done();
                });
        });
    });
    
    /*
      * Test the /PATCH route
      */
    describe('/PATCH Player', () => {
        it('it should PATCH player ', (done) => {
            const player = {
                name: 'Name',
                surname: 'Surname',
                birthdate: '1999-01-01',
                status: 'Active',
                gender: 'Male',
                nationality: 'Canada',
                height: '185 cm',
                weight: '85 kg',
                shoots: 'Left'
            };
            models.Player.create(player).then((savedPlayer) => {
                // console.log(JSON.stringify(savedPlayer, undefined, 2));
                const updatePlayer = {
                    name: 'Name2',
                    surname: 'Surname2'
                };
                chai.request(server)
                    .patch(`/players/${savedPlayer.id}/update`)
                    .send(updatePlayer)
                    .end((err, res) => {
                        // console.log(JSON.stringify(res.body, undefined, 2));
                        res.should.have.status(200);
                        res.body.should.be.a('object');
                        res.body.success.should.be.a('boolean');
                        res.body.success.should.be.eql(true);
                        done();
                    });
            }).catch((error) => {
                console.log(`Error ${error.errors[0].message}`);
                done();
            });
            
        });
    });
    
    /*
      * Test the /PATCH route
      */
    describe('/PATCH Player', () => {
        it('it should not PATCH player ', (done) => {
            const player = {
                name: 'Name',
                surname: 'Surname',
                birthdate: '1999-01-01',
                status: 'Active',
                gender: 'Male',
                nationality: 'Canada',
                height: '185 cm',
                weight: '85 kg',
                shoots: 'Left'
            };
            models.Player.create(player).then((savedPlayer) => {
                // console.log(JSON.stringify(savedPlayer, undefined, 2));
                const updatePlayer = {
                    name: null
                };
                chai.request(server)
                    .patch(`/players/${savedPlayer.id}/update`)
                    .send(updatePlayer)
                    .end((err, res) => {
                        // console.log(JSON.stringify(res.body, undefined, 2));
                        res.should.have.status(200);
                        res.body.should.be.a('object');
                        res.body.success.should.be.a('boolean');
                        res.body.success.should.be.eql(false);
                        res.body.message.should.be.a('string');
                        res.body.message.should.be.eql('Player.name cannot be null');
                        done();
                    });
            }).catch((error) => {
                console.log(`Error ${error.errors[0].message}`);
                done();
            });
            
        });
    });
    
    /*
      * Test the /PATCH route
      */
    describe('/DELETE Player', () => {
        it('it should DELETE player ', (done) => {
            const player = {
                name: 'Name',
                surname: 'Surname',
                birthdate: '1999-01-01',
                status: 'Active',
                gender: 'Male',
                nationality: 'Canada',
                height: '185 cm',
                weight: '85 kg',
                shoots: 'Left'
            };
            models.Player.create(player).then((savedPlayer) => {
                // console.log(JSON.stringify(savedPlayer, undefined, 2));
                chai.request(server)
                    .delete(`/players/${savedPlayer.id}/delete`)
                    .end((err, res) => {
                        // console.log(JSON.stringify(res.body, undefined, 2));
                        res.should.have.status(200);
                        res.body.should.be.a('object');
                        res.body.success.should.be.a('boolean');
                        res.body.success.should.be.eql(true);
                        done();
                    });
            }).catch((error) => {
                console.log(`Error ${error.errors[0].message}`);
                done();
            });
            
        });
    });
    
    /*
      * Test the /PATCH route
      */
    describe('/DELETE Player', () => {
        it('it should DELETE player ', (done) => {
            const player = {
                name: 'Name',
                surname: 'Surname',
                birthdate: '1999-01-01',
                status: 'Active',
                gender: 'Male',
                nationality: 'Canada',
                height: '185 cm',
                weight: '85 kg',
                shoots: 'Left'
            };
            models.Player.create(player).then(() => {
                // console.log(JSON.stringify(savedPlayer, undefined, 2));
                chai.request(server)
                    .delete(`/players/1000/delete`)
                    .end((err, res) => {
                        // console.log(JSON.stringify(res.body, undefined, 2));
                        res.should.have.status(200);
                        res.body.should.be.a('object');
                        res.body.success.should.be.a('boolean');
                        res.body.success.should.be.eql(true);
                        done();
                    });
            }).catch((error) => {
                console.log(`Error ${error.errors[0].message}`);
                done();
            });
            
        });
    });
    
});
